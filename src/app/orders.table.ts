import { Component, Input } from '@angular/core';
import Order from './order';
import { OrdersMiddleware } from './orders.middleware';
import CartItem from './cart.item';
import {OrderService} from './orders.service';

@Component({
  selector: 'OrdersTableComponent',
  styleUrls: ['./orders.table.css'],
  template:
      `
    <div id="table-contents">
      <div id="orders">
        <div
          class="order"
          *ngFor="let singleOrder of orders"
          [ngClass]="{'selected': singleOrder.id === selectedOrder.id}">
          <div class="row">
            <div class="label">id:</div>
            <div>{{singleOrder.id}}</div>
          </div>
          <div class="row">
            <div class="label">adresat:</div>
            <div>{{singleOrder.name}}</div>
            <div
              [ngClass]="{'hidden': editedOrderId !== singleOrder.id}"
              class="edit">
              <input
                #editName
                type="text"
                class="edit-input-long">
            </div>
          </div>
          <div class="row">
            <div class="label">adres:</div>
            <div>{{singleOrder.address}}</div>
            <div
              [ngClass]="{'hidden': editedOrderId !== singleOrder.id}"
              class="edit">
              <input
                #editAddress
                type="text"
                class="edit-input-long">
            </div>
          </div>
          <div class="row">
            <div class="label">Wartość zamówienia:</div>
            <div>{{singleOrder.totalValue}}</div>
            <div
              [ngClass]="{'hidden': editedOrderId !== singleOrder.id}"
              class="edit">
              <input
                #editTotalValue
                type="number"
                class="edit-input-long">
            </div>
          </div>
          <button
            (click)="showProducts(singleOrder)">Pokaż produkty
          </button>
          <div class="row">
            <button
              (click)="deleteOrder(singleOrder)">
              Usuń
            </button>
            <button
              (click)="editOrder(singleOrder)">
              Edytuj
            </button>
            <button
              [ngClass]="{'hidden': editedOrderId !== singleOrder.id}"
              class="big-margin"
              (click)="saveOrderChanges(singleOrder, editName.value, editAddress.value, editTotalValue.value)">
              Zapisz
            </button>
          </div>
        </div>
      </div>
      <div id="products">
        <div
          class="product"
          *ngFor="let item of items">
          <div class="row">
            <div class="label">id produktu:</div>
            <div>{{item.id}}</div>
          </div>
          <div class="row">
            <div class="label">ilość:</div>
            <div>{{item.amount}}</div>
            <div 
              id="amount-edit"
              [ngClass]="{'hidden': activeItem.id !== item.id}"
              class="edit">
              <input
                #editInput
                type="number"
                class="edit-input">
            </div>
          </div>
          <div class="row">
            <button
              (click)="deleteItem(item)">
              Usuń
            </button>
            <button
              (click)="editItem(item)">
              Edytuj
            </button>
            <button
              [ngClass]="{'hidden': activeItem.id !== item.id}"
              class="big-margin"
              (click)="saveNewAmount(item, editInput.value)">
              Zapisz
            </button>
          </div>
        </div>
      </div>
    </div>
  `
})

export class OrdersTableComponent {
  @Input() orders: Order[];
  @Input() selectedOrder: Order;

  activeItem: CartItem;
  editedOrderId = -1;

  items: CartItem[] = [];

  constructor(private orderMiddleware: OrdersMiddleware, private orderService: OrderService) {
    this.activeItem = {
      id: -1,
      amount: -1
    };
  }

  showProducts(order): void {
    this.selectedOrder = order;
    this.items = this.orderMiddleware.bindCartsForOrder(order);
  }

  deleteItem(item): void {
    this.orderMiddleware.deleteItem(this.selectedOrder, item);
    this.synchronizeWithServer();
    this.items = this.orderMiddleware.deleteFromArray(this.items, item);
  }

  editItem(item): void {
    this.activeItem = item;
  }

  saveNewAmount(item, value) {
    item.amount = value;
    this.orderMiddleware.editItem(this.selectedOrder, item);
  }

  synchronizeWithServer(): void {
    this.orderService.getOrdersData()
      .then(function (data) {
        this.orders = data;
      }.bind(this));
  }

  deleteOrder(order: Order) {
    this.orderMiddleware.deleteOrder(order);
  }

  editOrder(order: Order) {
    this.editedOrderId = order.id;
  }

  saveOrderChanges(order: Order, name, address, totalValue) {
    if (name && name !== '') {
      order.name = name;
    }

    if (address && address !== '') {
      order.address = address;
    }

    if (totalValue && totalValue !== '') {
      order.totalValue = totalValue;
    }

    this.orderMiddleware.editOrder(order);
  }
}
