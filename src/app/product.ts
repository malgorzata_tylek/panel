export class Product {
  id: number;
  name: string;
  description: string;
  price: number;
  currency: string;

  constructor(name: string, description: string, price: number) {
    this.name = name;
    this.description = description;
    this.price = price;
    this.currency = 'PLN';
  }
}
