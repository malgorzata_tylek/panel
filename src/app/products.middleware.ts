import { Injectable } from '@angular/core';
import { ProductService } from './products.service';
import { Category } from './category';
import { Product } from './product';

class CategoryDB {
  _id: number;
  id: number;
  name: string;
  products: string;

  constructor(category: Category, products: string) {
    this._id = category._id;
    this.id = category.id;
    this.name = category.name;
    this.products = products;
  }
}


@Injectable()
export class ProductsMiddleware {

  constructor(private productService: ProductService) {
  }

  deleteProduct(category: Category, product: Product) {
    let products = category.products;
    products = this.deleteFromArray(products, product);

    const jsonArray = JSON.stringify(products),
      categoryDB = new CategoryDB(category, jsonArray);

    this.productService.putCategory(categoryDB);
  }

  editProduct(category: Category, product: Product) {
    let products = category.products;
    products = this.deleteFromArray(products, product);

    products.push(product);

    const jsonArray = JSON.stringify(products),
      categoryDB = new CategoryDB(category, jsonArray);

    this.productService.putCategory(categoryDB);
  }

  addProduct(category: Category, product: Product) {
    const products = category.products;
    this.sortArray(products);

    product.id = products[products.length - 1].id + 1;
    products.push(product);

    const jsonArray = JSON.stringify(products),
      categoryDB = new CategoryDB(category, jsonArray);

    this.productService.putCategory(categoryDB);
  }

  deleteCategory(category: Category) {
    this.productService.deleteCategory(category);
  }

  editCategory(category: Category) {
    this.productService.putCategory(category);
  }

  addCategory(category: Category) {
    this.productService.sendCategory(category);
  }

  deleteFromArray(array, item) {
    return array
      .filter(order => order.id !== item.id);
  }

  sortArray(array) {
    array.sort(function(a, b) {
      if (a.id !== b.id) {
        return a.id - b.id;
      }
      if (a.name === b.name) {
        return 0;
      }
      return a.name > b.name ? 1 : -1;
    });
  }

}
