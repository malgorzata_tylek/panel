import { Product } from './product';

export class Category {
  _id: number;
  id: number;
  name: string;
  products: Product[];


  constructor(name: string, products: Product[]) {
    this.name = name;
    this.products = products;
  }
}
