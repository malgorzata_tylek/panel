import { Injectable } from '@angular/core';
import { Category } from './category';

import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import Order from './order';
import {catchError, tap} from "rxjs/operators";


@Injectable()
export class OrderService {

  constructor(private http: Http) {
  }

  getOrdersData(): Promise<Order[]> {
    return new Promise((resolve, reject) => {
      let data;
      this.getData()
        .subscribe((value) => {
          data = value;
          return data;
        }, function (err) {
          reject({err, data});
        }, function () {
          resolve(data);
        });
    });
  }

  getData(): Observable<Order[]> {
    const apiUrl = 'http://localhost:4202/rest/orders';

    const headers = new Headers();
    headers.append('Cache-Control', 'no-cache');
    headers.append('Content-Type', 'application/json');
    headers.append('x-apikey', '5a0ef7411ef3dc24313a7d2e');

    const options = new RequestOptions({headers, withCredentials: true});

    return this.http.get(apiUrl, options)
      .map((response: Response) => {
        console.warn('response', response);
        return response.json();
      });
  }

  sendOrder(data) {
    const apiUrl = 'http://localhost:4202/rest/orders';

    const headers = new Headers();
    headers.append('Cache-Control', 'no-cache');
    headers.append('Content-Type', 'application/json');
    headers.append('x-apikey', '5a0ef7411ef3dc24313a7d2e');

    const options = new RequestOptions({headers, withCredentials: true});

    return this.http.post(apiUrl, data, options)
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log('Error occured');
        }
      );
  }

  putOrder(newOrder) {
    const apiUrl = 'http://localhost:4202/rest/orders/' + newOrder._id;

    const headers = new Headers();
    headers.append('Cache-Control', 'no-cache');
    headers.append('Content-Type', 'application/json');
    headers.append('x-apikey', '5a0ef7411ef3dc24313a7d2e');

    const options = new RequestOptions({headers, withCredentials: true});

    return this.http.put(apiUrl, newOrder, options)
      .subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.log('Error occured');
      }
    );
  }

  deleteOrder(order: Order) {
    const apiUrl = 'http://localhost:4202/rest/orders/' + order._id;

    const headers = new Headers();
    headers.append('Cache-Control', 'no-cache');
    headers.append('Content-Type', 'application/json');
    headers.append('x-apikey', '5a0ef7411ef3dc24313a7d2e');

    const options = new RequestOptions({headers, withCredentials: true});

    return this.http.delete(apiUrl, options)
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log('Error occured');
        }
      );
  }
}
