import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Product } from './product';
import { ProductsMiddleware } from './products.middleware';
import { ProductService } from './products.service';
import { Category } from './category';

@Component({
    selector: 'ProductComponent',
    styleUrls: ['./product.component.css'],
    template:
    `
      <div id="containter">
          <div class="row">
            <div class="first-column">{{ product.name }}</div>
            <div
              [ngClass]="{'hidden': !edited}"
              class="edit">
              <input
                #editName
                type="text"
                class="edit-input-large">
            </div>
          </div>
          <div class="row">
            <div class="first-column">{{ product.description }}</div>
            <div
              [ngClass]="{'hidden': !edited}"
              class="edit">
              <input
                #editDescription
                type="text"
                class="edit-input-large">
            </div>
          </div>
          <div class="row">
            <div class="row first-column">
              <div>{{product.price}}</div>
              <div>{{product.currency}}</div>
            </div>
            <div
              [ngClass]="{'hidden': !edited}"
              class="edit">
              <input
                #editPrice
                type="number"
                class="edit-input-long">
            </div>
          </div>
          <div class="row">
            <button
              (click)="deleteProduct()">
              Usuń
            </button>
            <button
              (click)="editProduct()">
              Edytuj
            </button>
            <button
              (click)="saveProduct(editName.value, editDescription.value, editPrice.value)"
              [ngClass]="{'hidden': !edited}">
              Zapisz
            </button>
          </div>
      </div>
    `
})


export class ProductComponent {
  @Output() updateCartEmitter: EventEmitter<any> = new EventEmitter();
  @Input() product: Product;
  @Input() category: Category;

  edited: Boolean = false;

  constructor(private productsMiddleware: ProductsMiddleware, private productsService: ProductService) {
  }

  deleteProduct() {
    this.productsMiddleware.deleteProduct(this.category, this.product);
  }

  editProduct() {
    this.edited = true;
  }

  saveProduct(name, description, price) {
    if (name && name !== '') {
      this.product.name = name;
    }

    if (description && description !== '') {
      this.product.description = description;
    }

    if (price && price !== '') {
      this.product.price = price;
    }

    this.productsMiddleware.editProduct(this.category, this.product);
  }
}
