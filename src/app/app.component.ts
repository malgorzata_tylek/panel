import { Component, OnInit } from '@angular/core';
import { ProductService } from './products.service';
import { Category } from './category';
import { OrderService } from './orders.service';
import Order from './order';

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.css'],
  template:
  `
    <div id="title-header">
      <div>{{title | uppercase}}</div>
    </div>
    <div class="main-container">
      <div id="content">
        <div id="tables">
          <button
            class="table"
            [ngClass]="{'selected': productsClicked}"
            (click)="onProductsClicked()">
            Produkty
          </button>
          <button
            class="table"
            [ngClass]="{'selected': ordersClicked}"
            (click)="onOrdersClicked()">
            Zamówienia
          </button>
        </div>
        <div id="table-contents">
          <ProductsTableComponent
            [categories]=categories
            [selectedCategory]="categories[0]"
            [ngClass]="{'hidden': !productsClicked}"
            class="products">
          </ProductsTableComponent>
          <OrdersTableComponent
            [orders]=orders
            [selectedOrder]="selectedOrder"
            [ngClass]="{'hidden': !ordersClicked}"
            class="orders">
          </OrdersTableComponent>
        </div>
      </div>
    </div>
  `
})
export class AppComponent implements OnInit {
  title = 'sklep sportowy - panel administratora';

  categories: Category[];
  orders: Order[];

  selectedOrder: Order;

  productsClicked: Boolean = false;
  ordersClicked: Boolean = false;

  constructor(private orderService: OrderService, private productService: ProductService) {
  }

  ngOnInit(): void {
    this.categories = [];
    this.orders = [];

    this.orderService.getOrdersData()
      .then(function (data) {
        this.orders = data;
      }.bind(this));

    this.productService.getCategoriesData()
      .then(function (data) {
        this.categories = data;
      }.bind(this));
  }

  onProductsClicked(): void {
    this.productsClicked = true;
    this.ordersClicked = false;
  }

  onOrdersClicked(): void {
    this.productsClicked = false;
    this.ordersClicked = true;
    this.selectedOrder = this.orders[0];
  }
}
