import { Component, Input } from '@angular/core';
import { Category } from './category';
import { ProductsMiddleware } from './products.middleware';
import { ProductService } from './products.service';
import { Product } from './product';

@Component({
  selector: 'ProductsTableComponent',
  styleUrls: ['./products.table.css'],
  template:
      `
    <div id="table-contents">
      <div id="categories">
        <button
          class="category"
          (click)="addNewCategory()">
          Dodaj kategorię
        </button>
        <div
          [ngClass]="{'hidden': !newCategory}">
          <div
            class="new-category-form">
            
            <div class="label">Nazwa kategorii</div>
            <div class="edit">
              <input
                #newCatName
                type="text"
                class="edit-input-middle">
            </div>
            <div class="label">Produkty (w postaci json)</div>
            <div class="edit">
              <textarea 
                #newProducts
                type="text"
                class="area-long">
              </textarea>
            </div>
            <button
              class="save-button"
              (click)="saveNewCategory(newCatName.value, newProducts.value)">
              Zapisz
            </button>
          </div>
        </div>
        <button
          class="category"
          *ngFor="let singleCategory of categories"
          [ngClass]="{'selected': singleCategory.id === selectedCategory.id}"
          (click)="showProductsOfCategory(singleCategory.id)">
          <div>
            <div class="row center">
              <div>{{singleCategory.name}}</div>
              <div
                [ngClass]="{'hidden':singleCategory.id !== editedCategoryId}"
                class="edit">
                <input
                  #editName
                  type="text"
                  class="edit-input-long">
              </div>
            </div>
            <div class="row center">
              <button
                (click)="deleteCategory(singleCategory)">
                Usuń
              </button>
              <button
                (click)="editCategory(singleCategory)">
                Edytuj
              </button>
              <button
                (click)="saveCategory(editName.value)"
                [ngClass]="{'hidden': singleCategory.id !== editedCategoryId}">
                Zapisz
              </button>
            </div>
          </div>
        </button>
      </div>
      <div id="products">
        <button 
          class="product add-product"
          (click)="addNewProduct()">
          Dodaj produkt
        </button>
        <div
          [ngClass]="{'hidden': !newProduct}">
          <div
            class="new-product-form">
            <div class="row ">
              <div class="label">Nazwa produktu</div>
              <div
                class="edit">
                <input
                  #newName
                  type="text"
                  class="edit-input-large">
              </div>
            </div>
            <div class="row ">
              <div class="label">Opis produktu</div>
              <div
                class="edit">
                <input
                  #newDescription
                  type="text"
                  class="edit-input-large">
              </div>
            </div>
            <div class="row ">
              <div class="label">Cena produktu</div>
              <div
                class="edit">
                <input
                  #newPrice
                  type="number"
                  class="edit-input-long">
              </div>
            </div>  
            <button
              class="save-button"
              (click)="saveNewProduct(newName.value, newDescription.value, newPrice.value)">
              Zapisz
            </button>
          </div>
        </div>
        <ProductComponent
          class="product"
          *ngFor="let singleProduct of selectedCategory.products"
          [category] = selectedCategory
          [product] = singleProduct >
        </ProductComponent>
      </div>
    </div>
  `
})

export class ProductsTableComponent {
  @Input() categories: Category[];
  @Input() selectedCategory: Category;

  editedCategoryId = -1;
  newProduct = false;
  newCategory = false;

  constructor(private productsMiddleware: ProductsMiddleware, private productsService: ProductService) {
  }

  showProductsOfCategory(id: number): void {
    this.selectedCategory = this.getCategoryById(id);
  }

  getCategoryById(id: number): Category {
    const found = this.categories.filter((category) => category.id === id)[0];

    return found ? found : this.selectedCategory;
  }

  deleteCategory(category) {
    this.productsMiddleware.deleteCategory(category);
  }

  editCategory(category) {
    this.editedCategoryId = category.id;
  }

  saveCategory(name) {
    this.editedCategoryId = -1;

    if (name && name !== '') {
      this.selectedCategory.name = name;
    }

    this.productsMiddleware.editCategory(this.selectedCategory);
  }

  saveNewProduct(name, desc, price) {
    this.newProduct = false;
    const product = new Product(name, desc, price);
    this.productsMiddleware.addProduct(this.selectedCategory, product);
  }

  addNewProduct() {
    this.newProduct = true;
  }

  addNewCategory() {
    this.newCategory = true;
  }

  saveNewCategory(newNameCat, newProducts) {
    this.newCategory = false;

    const category = new Category(newNameCat, newProducts);
    this.productsMiddleware.addCategory(category);
  }
}
