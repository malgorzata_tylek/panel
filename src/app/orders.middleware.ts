import { Injectable } from '@angular/core';
import { OrderService } from './orders.service';
import CartItem from './cart.item';
import Order from './order';

class OrderDB {
  _id: number;
  id: number;
  name: string;
  address: number;
  totalValue: string;
  cart: string;

  constructor(order: Order, jsonString: string) {
    this._id = order._id;
    this.id = order.id;
    this.name = order.name;
    this.address = order.address;
    this.totalValue = order.totalValue;
    this.cart = jsonString;
  }
}

@Injectable()
export class OrdersMiddleware {

  constructor(private orderService: OrderService) {
  }

  deleteItem(order: Order, item: CartItem) {
    let items = this.bindCartsForOrder(order);
    items = this.deleteFromArray(items, item);

    const jsonArray = JSON.stringify(items),
      orderDB = new OrderDB(order, jsonArray);

    this.orderService.putOrder(orderDB);
  }

  editItem(order: Order, item: CartItem) {
    let items = this.bindCartsForOrder(order);
    items = this.deleteFromArray(items, item);

    items.push(item);

    const jsonArray = JSON.stringify(items),
      orderDB = new OrderDB(order, jsonArray);

    this.orderService.putOrder(orderDB);
  }

  deleteOrder(order: Order) {
    this.orderService.deleteOrder(order);
  }

  editOrder(order: Order) {
    this.orderService.putOrder(order);
  }

  bindCartsForOrder(order): CartItem[] {
    return Array.of(order.cart)[0];
  }

  deleteFromArray(array, item) {
    return array
      .filter(order => order.id !== item.id);
  }

}
