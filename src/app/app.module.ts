import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ProductService } from './products.service';
import { OrderService } from './orders.service';

import { HttpModule } from '@angular/http';
import { ProductsTableComponent } from './products.table';
import { ProductComponent } from './product.component';
import { OrdersTableComponent } from './orders.table';
import { OrdersMiddleware } from './orders.middleware';
import { ProductsMiddleware } from './products.middleware';

@NgModule({
  declarations: [
    AppComponent,
    ProductsTableComponent,
    ProductComponent,
    OrdersTableComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [ProductService, ProductsMiddleware, OrderService, OrdersMiddleware],
  bootstrap: [AppComponent]
})
export class AppModule { }
