export default class Order {
  _id: number;
  id: number;
  name: string;
  address: number;
  totalValue: string;
  cart: object;

  constructor(_id: number, id: number, name: string, address: number, totalValue: string, cart: Object) {
    this._id = _id;
    this.id = id;
    this.name = name;
    this.address = address;
    this.totalValue = totalValue;
    this.cart = cart;
  }
}
